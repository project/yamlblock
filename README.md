
Contents of this file
---------------------

 * About this module
 * Demo
 * Installation


About this Module
-----------------

Provides a base block that allows block configuration settings to be edited 
and maintained using YAML.

For custom blocks that don't require an admin UI, it can be easier to just 
enter YAML data that is parsed in to variables that can be used to render a 
custom block.

This module is for sites that have... 

- Custom modules
- Lots of custom blocks
- Lots of custom markup/templates for their custom blocks

Examples of custom blocks that could benefit from using YAML.

- A list of custom links that don't have to maintained by Drupal's menu system.
- Custom blocks that contain a lot of custom text and labels that need to 
  be continuously edited.
- Custom data sets for a static table of information.

A YAML links block is provided to illustrate the potential of using YAML as the 
data behind a custom block. The YAML links block just takes a list of links,
processes them, and renders them as a simple list of links.

Below is an example of the YAML data used by YAML links block. 

    # Links to D.O. that open in new window.
    drupal:
      title: 'Drupal.org'
      url: 'http://drupal.org'
      attributes:
        target: '_blank'
    # Opens search in a modal dialog.
    search:
      title: 'Search'
      route_name: 'search.view_node_search'
      dialog: 'modal'      
    # Opens contact in a modal dialog.
    contact:
      title: 'Contact'
      route_name: 'contact.site_page'
      destination: true
      dialog: true
      dialog_options: 
        width: '600px'
    # Opens user login in a modal dialog and redirects the user to the current page.
    login:
      title: 'Login'
      route_name: 'user.login'
      destination: true
      dialog: 'modal'
    # Logs out the current user.
    logout:
      title: 'Logout'
      route_name: 'user.logout'


Demo
----
 
> [Watch a demo](http://youtu.be/o_Ppg7b4mPM) of the YAML block module.
 
 
Installation
------------

1. Copy/upload the 'yamlblock.module' to the modules directory of your Drupal
   installation.
2. Enable the 'YAML block' module in 'Extend'. (/admin/modules)
3. Place the 'YAML links' block on your site. (/admin/structure/block)


Author/Maintainer
-----------------

- [Jacob Rockowitz](http://drupal.org/user/371407)
