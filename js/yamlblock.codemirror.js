/**
 * @file
 * Javascript behaviors for YAML block CodeMirror integration.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.yamlBlockCodeMirror = {
    attach: function (context) {
      // YAML block CodeMirror editor.
      $(context).find('textarea.yamlblock-codemirror').once('yamlblock-codemirror').each(function () {
        // #59 HTML5 required attribute breaks hack for form submission.
        // https://github.com/marijnh/CodeMirror-old/issues/59
        $(this).removeAttr('required');

        CodeMirror.fromTextArea(this, {
          mode: 'text/x-yaml',
          lineNumbers: true,
          tabSize: 2,
          indentWithTabs: false,
          viewportMargin: Infinity,
          readOnly: $(this).prop('readonly') ? true : false,
          // Setting for using spaces instead of tabs - https://github.com/codemirror/CodeMirror/issues/988
          extraKeys: {
            Tab: function (cm) {
              var spaces = Array(cm.getOption("indentUnit") + 1).join(" ");
              cm.replaceSelection(spaces);
            }
          }
        });
      });

      // YAML form CodeMirror syntax coloring.
      $(context).find('.yamlblock-codemirror-runmode').once('yamlblock-codemirror').each(function () {
        // Mode Runner  - http://codemirror.net/demo/runmode.html
        CodeMirror.runMode($(this).addClass('cm-s-default').html(), 'text/x-yaml', this);
      });

    }
  };

})(jQuery, Drupal);
