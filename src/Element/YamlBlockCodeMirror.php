<?php

/**
 * @file
 * Contains \Drupal\yamlblock\Element\YamlBlockCodeMirror.
 */

namespace Drupal\yamlblock\Element;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Render\Element\Textarea;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form element for input of YAML text using CodeMirror.
 *
 * @FormElement("yamlblock_codemirror")
 */
class YamlBlockCodeMirror extends Textarea {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#cols' => 60,
      '#rows' => 5,
      '#resizable' => 'vertical',
      '#process' => [
        [$class, 'processYamlBlockCodeMirror'],
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderYamlBlockCodeMirror'],
        [$class, 'preRenderGroup'],
      ],
      '#element_validate' => [
        [$class, 'validateYamlBlockCodeMirror'],
      ],
      '#theme' => 'textarea',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes a 'yamlblock_codemirror' element.
   */
  public static function processYamlBlockCodeMirror(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#attached']['library'][] = 'yamlblock/yamlblock.codemirror';
    return $element;
  }

  /**
   * Form element validation handler for #type 'yamlblock_codemirror'.
   */
  public static function validateYamlBlockCodeMirror(&$element, FormStateInterface $form_state, &$complete_form) {
    try {
      $value = trim($element['#value']);
      $data = Yaml::decode($value);
      if (!is_array($data) && $value) {
        throw new \Exception(t('YAML must contain an associative array of inputs.'));
      }
    }
    catch (\Exception $exception) {
      $form_state->setError($element, t('%title is not valid. @message', ['%title' => $element['#title'], '@message' => $exception->getMessage()]));
    }
  }

  /**
   * Prepares a #type 'yamlblock_codemirror' render element for theme_input().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_input().
   */
  public static function preRenderYamlBlockCodeMirror($element) {
    static::setAttributes($element, ['yamlblock-codemirror']);
    return $element;
  }

}
