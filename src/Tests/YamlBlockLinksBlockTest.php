<?php

/**
 * @file
 * Definition of Drupal\yamlblock\Tests\YamlBlockLinksBlockTest.
 */

namespace Drupal\yamlblock\Tests;

use Drupal\Core\Url;
use Drupal\simpletest\WebTestBase;

/**
 * Tests YAML block links.
 *
 * @group YamlBlock
 */
class YamlBlockLinksBlockTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['block', 'user', 'node', 'yamlblock', 'dblog'];

  /**
   * A test user with administrative privileges.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Tests block rendering.
   */
  protected function testYamlLinksBlock() {
    // Create a node (nid:1).
    $content_type = $this->createContentType();
    $node = $this->createNode(['type' => $content_type->id()]);

    $data = <<<EOF
# URL
url:
  title: 'URL'
  url: 'http://drupal.org'
  attributes:
    target: '_blank'

# URL
url_with_query:
  title: 'URL'
  url: 'http://drupal.org'
  options:
    query:
      param: test

# Route name
route_name:
  title: 'Home'
  route_name: '<front>'

# Route name with parameters
route_parameters:
  title: 'USER:1'
  route_name: 'entity.user.canonical'
  route_parameters:
    user: 1

# Node id (nid)
node:
  title: 'Node: 1'
  nid: 1

# Modal dialog
modal_dialog:
  title: 'User login (modal)'
  route_name: 'user.login'
  dialog: 'modal'
  dialog_options:
    width: '200px'
    height: '200px'
  destination: true

# Role
role:
  title: 'Role (authenticated)'
  url: 'http://drupal.org'
  roles:
    - authenticated

# Permission
permission:
  title: 'Permission (administer site configuration)'
  url: 'http://drupal.org'
  permission: 'administer site configuration'

# Broken
broken:
  title: 'Broken'
  route_name: 'broken'
EOF;
    $block = $this->drupalPlaceBlock('yamlblock_links_block');
    $block->getPlugin()->setConfigurationValue('data', $data);
    $block->save();

    // Get front page.
    $this->drupalGet('');

    // Check URL.
    $this->assertRaw('<li class="url"><a href="http://drupal.org" target="_blank">URL</a></li>');

    // Check query string.
    $this->assertRaw('<li class="url-with-query"><a href="http://drupal.org?param=test">URL</a></li>');

    // Check route.
    $this->assertRaw('<li class="route-name"><a href="' . base_path() . '">Home</a></li>');

    // Check nid.
    $this->assertRaw('<li class="node"><a href="' . $node->toUrl()->toString() . '">Node: 1</a></li>');

    // Check modal and destination.
    $this->drupalGet('node/1');
    $url = Url::fromRoute('user.login', [], ['query' => ['destination' => $node->toUrl()->toString()]]);
    $this->assertRaw('<li class="modal-dialog"><a href="' . $url->toString() . '" class="use-ajax" data-accepts="application/vnd.drupal-modal" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:&quot;200px&quot;,&quot;height&quot;:&quot;200px&quot;}">User login (modal)</a></li>');

    // Check admin route access denied.
    $url = Url::fromRoute('entity.user.canonical', ['user' => 1]);
    $this->assertNoRaw('<li class="route-parameters"><a href="' . $url->toString() . '">USER:1</a></li>');

    // Check roles and permission access denied.
    $this->assertNoRaw('<li class="role"><a href="http://drupal.org">Role (authenticated)</a></li>');
    $this->assertNoRaw('<li class="permission"><a href="http://drupal.org">Permission (administer site configuration)</a></li>');

    // Assert that a watchdog message was logged by YAML block links.
    $status = (bool) db_query_range("SELECT 1 FROM {watchdog} WHERE type = 'yamlblock' AND message LIKE 'Broken link%'", 0, 1);
    $this->assert($status, 'A watchdog message was logged by YAML block links.');

    // Login to check access controls.
    $admin_user = $this->drupalCreateUser([
      'access content',
      'access administration pages',
      'administer users',
      'administer blocks',
      'administer site configuration',
    ]);
    $this->drupalLogin($admin_user);

    // Get front page.
    $this->drupalGet('');

    // Check admin route access granted.
    $url = Url::fromRoute('entity.user.canonical', ['user' => 1]);
    $this->assertRaw('<li class="route-parameters"><a href="' . $url->toString() . '">USER:1</a></li>');

    // Check roles and permission access granted.
    $this->assertRaw('<li class="role"><a href="http://drupal.org">Role (authenticated)</a></li>');
    $this->assertRaw('<li class="permission"><a href="http://drupal.org">Permission (administer site configuration)</a></li>');

  }

}
