<?php

/**
 * @file
 * Contains \Drupal\yamlblock\Plugin\Block\YamlBlockLinksBlock.
 */

namespace Drupal\yamlblock\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides the 'YAML links' block.
 *
 * @Block(
 *   id = "yamlblock_links_block",
 *   admin_label = @Translation("YAML links"),
 *   category = @Translation("YAML block")
 * )
 */
class YamlBlockLinksBlock extends YamlBlockBase {

  /**
   * Assets to be attached to this block.
   *
   * @var array
   */
  protected $attached = [];

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Add examples of links.
    $form['example'] = [
      '#type' => 'details',
      '#title' => $this->t('Example'),
    ];
    $form['example']['yaml'] = [
      '#markup' => htmlentities(file_get_contents(drupal_get_path('module', 'yamlblock') . '/config/examples/yamlblock_links_block.yml')),
      '#prefix' => '<pre class="yamlblock-codemirror-runmode">',
      '#suffix' => '</pre>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'links',
      '#links' => $this->initLinks($this->getData()),
      '#attached' => $this->attached,
    ];
  }

  /**
   * Initialize an array of links.
   *
   * @param array $links
   *   An array of links.
   *
   * @return array
   *   An array of links.
   */
  protected function initLinks(array $links = []) {
    if (!is_array($links)) {
      return $links;
    }

    foreach ($links as $key => $link) {
      $links[$key] = $this->initLink($link);
      if (empty($links[$key])) {
        unset($links[$key]);
      }
    }

    return $links;
  }

  /**
   * Initialize a link.
   *
   * @param array $link
   *   An associative array representing a link.
   *
   * @return array
   *   An associative array representing a link.
   */
  protected function initLink(array $link = []) {
    if (!is_array($link)) {
      return $link;
    }

    // Set default values.
    $link += [
      'title' => '',
      'url' => NULL,
      'route_name' => NULL,
      'route_parameters' => [],
      'nid' => NULL,
      'options' => [],
      'destination' => FALSE,
      'attributes' => [],
      'dialog' => FALSE,
      'dialog_options' => [],
      'permission' => '',
      'roles' => [],
    ];

    // Check custom role access.
    if ($link['roles'] && !array_intersect($link['roles'], \Drupal::currentUser()->getRoles())) {
      return FALSE;
    }

    // Check custom permission access.
    if ($link['permission'] && !\Drupal::currentUser()->hasPermission($link['permission'])) {
      return FALSE;
    }

    // Set dialog attributes.
    if ($link['dialog']) {
      $link['attributes']['class'][] = 'use-ajax';
      $link['attributes']['data-accepts'] = 'application/vnd.drupal-modal';
      $link['attributes']['data-dialog-type'] = ($link['dialog'] == 'modal') ? 'modal' : 'dialog';
      if ($link['dialog_options']) {
        $link['attributes']['data-dialog-options'] = json_encode($link['dialog_options']);
      }
      $this->attached['library'][] = 'core/drupal.ajax';
      $this->attached['library'][] = 'core/drupal.dialog';
    }

    // Set url.
    if ($link['url']) {
      // From href.
      $link['url'] = \Drupal::pathValidator()->getUrlIfValid($link['url']);
    }
    elseif (is_numeric($link['nid'])) {
      // From node id (nid).
      $link['url'] = Url::fromRoute('entity.node.canonical', ['node' => $link['nid']]);
    }
    elseif ($link['route_name']) {
      $link['url'] = Url::fromRoute($link['route_name'], $link['route_parameters']);
    }

    if ($link['url'] instanceof Url) {
      // Set options.
      if ($link['options']) {
        $link['url']->setOptions($link['options']);
      }

      // Append destination to query options.
      if ($link['destination']) {
        $query = $link['url']->getOption('query') ?: [];
        $query += \Drupal::destination()->getAsArray();
        $link['url']->setOption('query', $query);
      }
    }

    // Make sure the URL is valid.
    try {
      if (!($link['url'] instanceof  Url)) {
        throw new \Exception('Missing URL for YAML block link');
      }
      $link['url']->toString();
    }
    catch (\Exception $exception) {
      // Log broken links and display a warning message to
      // block administrators.
      $theme = \Drupal::theme()->getActiveTheme()->getName();
      $url = Url::fromRoute('block.admin_display_theme', ['theme' => $theme]);
      $t_args = [
        ':href' => $url->toString(),
        '@title' => $link['title'],
        '@block_title' => $this->configuration['label'],
        '@theme' => $theme,
      ];
      \Drupal::logger('yamlblock')->notice(
        "Broken link to '@title' detected in '@block_title' (@theme)",
        $t_args + ['link' => Link::fromTextAndUrl($this->t('Edit'), $url)->toString()]
      );
      if (\Drupal::currentUser()->hasPermission('administer blocks')) {
        drupal_set_message($this->t("Broken link to '@title' detected in '<a href=\":href\">@block_title</a>' (@theme)", $t_args), 'warning');
      }
      return FALSE;
    }

    // Check URL access.
    if (!$link['url']->access()) {
      return FALSE;
    }

    // Return only valid link properties.
    return array_intersect_key(
      $link,
      ['title' => 'title', 'url' => 'url', 'attributes' => 'attributes']
    );
  }

}
