<?php

/**
 * @file
 * Contains \Drupal\yamlblock\Plugin\Block\YamlBlockBase.
 */

namespace Drupal\yamlblock\Plugin\Block;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base block that allows block configuration settings to be edited using YAML.
 */
abstract class YamlBlockBase extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'data' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['data'] = [
      '#type' => 'yamlblock_codemirror',
      '#title' => $this->t('Data (YAML)'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['data'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['data'] = $form_state->getValue('data');
  }

  /**
   * Get configuration data settings.
   */
  protected function getData() {
    return Yaml::decode($this->configuration['data']);
  }

}
